#include <cmath>
#include <numeric>
#include <rili/noise/Perlin.hpp>

namespace {
inline double fade(double t) { return t * t * t * (t * (t * 6 - 15) + 10); }
inline double lerp(double a, double b, double x) { return a + x * (b - a); }
inline double grad(int hash, double x, double y, double z) {
    const int h = hash % 16;
    const double u = h < 8 ? x : y;
    const double v = h < 4 ? y : h == 12 || h == 14 ? x : z;
    return ((h % 2) == 0 ? u : -u) + ((h % 3) == 0 ? v : -v);
}
}  // namespace
namespace rili {
namespace noise {
Perlin::Perlin()
    : m_gradients{
          151, 160, 137, 91,  90,  15,  131, 13,  201, 95,  96,  53,  194, 233, 7,   225, 140, 36,  103, 30,  69,  142,
          8,   99,  37,  240, 21,  10,  23,  190, 6,   148, 247, 120, 234, 75,  0,   26,  197, 62,  94,  252, 219, 203,
          117, 35,  11,  32,  57,  177, 33,  88,  237, 149, 56,  87,  174, 20,  125, 136, 171, 168, 68,  175, 74,  165,
          71,  134, 139, 48,  27,  166, 77,  146, 158, 231, 83,  111, 229, 122, 60,  211, 133, 230, 220, 105, 92,  41,
          55,  46,  245, 40,  244, 102, 143, 54,  65,  25,  63,  161, 1,   216, 80,  73,  209, 76,  132, 187, 208, 89,
          18,  169, 200, 196, 135, 130, 116, 188, 159, 86,  164, 100, 109, 198, 173, 186, 3,   64,  52,  217, 226, 250,
          124, 123, 5,   202, 38,  147, 118, 126, 255, 82,  85,  212, 207, 206, 59,  227, 47,  16,  58,  17,  182, 189,
          28,  42,  223, 183, 170, 213, 119, 248, 152, 2,   44,  154, 163, 70,  221, 153, 101, 155, 167, 43,  172, 9,
          129, 22,  39,  253, 19,  98,  108, 110, 79,  113, 224, 232, 178, 185, 112, 104, 218, 246, 97,  228, 251, 34,
          242, 193, 238, 210, 144, 12,  191, 179, 162, 241, 81,  51,  145, 235, 249, 14,  239, 107, 49,  192, 214, 31,
          181, 199, 106, 157, 184, 84,  204, 176, 115, 121, 50,  45,  127, 4,   150, 254, 138, 236, 205, 93,  222, 114,
          67,  29,  24,  72,  243, 141, 128, 195, 78,  66,  215, 61,  156, 180, 151, 160, 137, 91,  90,  15,  131, 13,
          201, 95,  96,  53,  194, 233, 7,   225, 140, 36,  103, 30,  69,  142, 8,   99,  37,  240, 21,  10,  23,  190,
          6,   148, 247, 120, 234, 75,  0,   26,  197, 62,  94,  252, 219, 203, 117, 35,  11,  32,  57,  177, 33,  88,
          237, 149, 56,  87,  174, 20,  125, 136, 171, 168, 68,  175, 74,  165, 71,  134, 139, 48,  27,  166, 77,  146,
          158, 231, 83,  111, 229, 122, 60,  211, 133, 230, 220, 105, 92,  41,  55,  46,  245, 40,  244, 102, 143, 54,
          65,  25,  63,  161, 1,   216, 80,  73,  209, 76,  132, 187, 208, 89,  18,  169, 200, 196, 135, 130, 116, 188,
          159, 86,  164, 100, 109, 198, 173, 186, 3,   64,  52,  217, 226, 250, 124, 123, 5,   202, 38,  147, 118, 126,
          255, 82,  85,  212, 207, 206, 59,  227, 47,  16,  58,  17,  182, 189, 28,  42,  223, 183, 170, 213, 119, 248,
          152, 2,   44,  154, 163, 70,  221, 153, 101, 155, 167, 43,  172, 9,   129, 22,  39,  253, 19,  98,  108, 110,
          79,  113, 224, 232, 178, 185, 112, 104, 218, 246, 97,  228, 251, 34,  242, 193, 238, 210, 144, 12,  191, 179,
          162, 241, 81,  51,  145, 235, 249, 14,  239, 107, 49,  192, 214, 31,  181, 199, 106, 157, 184, 84,  204, 176,
          115, 121, 50,  45,  127, 4,   150, 254, 138, 236, 205, 93,  222, 114, 67,  29,  24,  72,  243, 141, 128, 195,
          78,  66,  215, 61,  156, 180} {}

Perlin::Perlin(std::function<std::uint64_t()> rng) {
    std::uint8_t gradient[sizeof(m_gradients) / 2];
    std::iota(gradient, gradient + sizeof(gradient), 0);
    for (std::uint32_t i = 0; i < sizeof(gradient); i++) {
        const auto s = rng();
        const auto d = rng();
        const auto tmp = gradient[s];
        gradient[s] = gradient[d];
        gradient[d] = tmp;
    }
    for (std::uint32_t i = 0; i < sizeof(m_gradients); i++) {
        m_gradients[i] = gradient[i % sizeof(gradient)];
    }
}

double Perlin::generate(double x, double y, double z) const {
    const auto X = std::uint8_t(x);
    const auto Y = std::uint8_t(y);
    const auto Z = std::uint8_t(z);

    x -= ::floor(x);
    y -= ::floor(y);
    z -= ::floor(z);

    const auto xf = fade(x);
    const auto yf = fade(y);
    const auto zf = fade(z);

    const auto G1 = m_gradients[X] + Y;
    const auto G2 = m_gradients[G1] + Z;
    const auto G3 = m_gradients[G1 + 1] + Z;
    const auto G4 = m_gradients[X + 1] + Y;
    const auto G5 = m_gradients[G4] + Z;
    const auto G6 = m_gradients[G4 + 1] + Z;

    const auto p1 = grad(m_gradients[G2], x, y, z);
    const auto p2 = grad(m_gradients[G5], x - 1, y, z);
    const auto p3 = grad(m_gradients[G2 + 1], x, y, z - 1);
    const auto p4 = grad(m_gradients[G5 + 1], x - 1, y, z - 1);
    const auto p5 = grad(m_gradients[G3], x, y - 1, z);
    const auto p6 = grad(m_gradients[G6], x - 1, y - 1, z);
    const auto p7 = grad(m_gradients[G3 + 1], x, y - 1, z - 1);
    const auto p8 = grad(m_gradients[G6 + 1], x - 1, y - 1, z - 1);

    const auto p9 = lerp(xf, p1, p2);
    const auto p10 = lerp(xf, p7, p8);
    const auto p11 = lerp(xf, p3, p4);
    const auto p12 = lerp(yf, p11, p10);
    const auto p13 = lerp(xf, p5, p6);
    const auto p14 = lerp(yf, p9, p13);
    return lerp(zf, p14, p12);
}

double Perlin::noise(double x, double y, double z, std::uint32_t numberOfOctaves, double amplitudeFactor) const {
    double result = 0.0;
    double amp = 1.0;

    for (std::uint32_t i = 0; i < numberOfOctaves; ++i) {
        result += generate(x, y, z) * amp;
        x *= 2.0;
        y *= 2.0;
        z *= 2.0;
        amp *= amplitudeFactor;
    }

    return result;
}

double Perlin::normalize(double v) { return v * 0.5 + 0.5; }

double noise::Perlin::noise(double x, double y, std::uint32_t numberOfOctaves, double amplitudeFactor) const {
    double result = 0.0;
    double amp = 1.0;

    for (std::uint32_t i = 0; i < numberOfOctaves; ++i) {
        result += generate(x, y) * amp;
        x *= 2.0;
        y *= 2.0;
        amp *= amplitudeFactor;
    }

    return result;
}

double noise::Perlin::noise(double x, std::uint32_t numberOfOctaves, double amplitudeFactor) const {
    double result = 0.0;
    double amp = 1.0;

    for (std::uint32_t i = 0; i < numberOfOctaves; ++i) {
        result += generate(x) * amp;
        x *= 2.0;
        amp *= amplitudeFactor;
    }

    return result;
}
}  // namespace noise
}  // namespace rili
