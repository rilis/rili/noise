#pragma once
#include <cstdint>
#include <functional>

namespace rili {
namespace noise {
class Perlin {
 public:
    Perlin();
    explicit Perlin(std::function<std::uint64_t()> rng);
    double noise(double x, double y, double z, std::uint32_t numberOfOctaves = 1, double amplitudeFactor = 0.5) const;
    double noise(double x, double y, std::uint32_t numberOfOctaves = 1, double amplitudeFactor = 0.5) const;
    double noise(double x, std::uint32_t numberOfOctaves = 1, double amplitudeFactor = 0.5) const;

    static double normalize(double v);

 private:
    double generate(double x, double y = 0, double z = 0) const;

 private:
    std::uint8_t m_gradients[512];
};
}  // namespace noise
}  // namespace rili
