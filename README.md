# Rili perlin

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of noise genenerators.
Implemented generator algorithms:
  *  Perlin (up to 3 dimensions)

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/noise)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/noise/badges/master/build.svg)](https://gitlab.com/rilis/rili/noise/commits/master)

